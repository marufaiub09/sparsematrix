#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "header/idx.h"
#include "header/common.h"
#include <assert.h>
#include <omp.h>

int NVirtual, bitshift;

double sparesify(char *filename)
{
	FILE *fp;
	fp = fopen(filename, "r");

	if (fp == NULL){
		printf("Could not open file!!");
		exit(0);
	}

	fscanf(fp, "%d %d %d", &M, &N, &nnz);

	// NVirtual and bitshift : Used to convert N into a power of 2
	// This is done to enable faster modulus and division
	NVirtual = 1;
	bitshift = 0;
	while(N>NVirtual) {
		NVirtual = NVirtual << 1;
		bitshift++;
	}

	L = (double *)malloc(nnz * sizeof(double *));
	IA = (long *)malloc(nnz * sizeof(long *));
	row_ptr = (int *)malloc((M+1) * sizeof(int *));

#ifdef SIZE
	int storage = nnz * ((sizeof(long *) + sizeof(double *)) + (M+1)*sizeof(int *));
	printf("Total Storage for IDX : %d bytes \n", storage);
#endif 

	int row, col; 
	for (int x = 0; x < nnz; x++) {
		fscanf(fp, "%d %d %lg\n ", &row, &col, &L[x]);
		IA[x] = (long)(row-1)*NVirtual + (col-1); 	// For 0-index array format
		row_ptr[row]++;	// to maintain 0-index array format
	}

	for (int x = 1; x <= M; x++){
		row_ptr[x] = row_ptr[x] + row_ptr[x-1];
	}

	fclose(fp);
}

double scalarMultiply(int q)
{
	double *P;
	P = (double *)malloc(nnz * sizeof(double *));
	for (int i = 0; i < nnz; i++) {
		P[i] = q * L[i];
	}
#ifdef DEBUG
	print_vector(P, M);
#endif
}

void vectorFile(char *vectorfile)
{
	FILE *fp;
	fp = fopen(vectorfile, "r");

	if (fp == NULL){
		printf("Could not open file to read vector!!");
		exit(0);
	}

	int temp;
	fscanf(fp, "%d %d", &temp, &temp);

	v = (double *)malloc(N * sizeof(double *));
	for (int x = 0; x < N; x++){
		fscanf(fp, "%lg ", &v[x]);
	}

	fclose(fp);
}

double vectorMultiply()
{ 
	double start_time = omp_get_wtime();

	double *temp;
	temp = (double *)malloc(M * sizeof(double *));
	int i, j, row, col;

	// method-1 without using row-ptr (DO NOT remove the commented code below)

#pragma omp parallel shared(temp, L, v, IA, NVirtual, bitshift), private(i, j, row, col)
	{
		/*
    // method-1 similar to coo but with index
#pragma omp for 
    for (i = 0; i < nnz; i++) {
    	// row = IA[i]/NVirtual;
    	// col = IA[i]%NVirtual;
    	row = IA[i]>>bitshift;
    	col = IA[i]&(NVirtual-1);
    	temp[row] += L[i] * v[col];
    }
		 */

		// method-2 using row-ptr
		int i;
#pragma omp for 
		for(i = 0; i < M ; i++){
			temp[i] = 0;
			for(j = row_ptr[i]; j < row_ptr[i+1]; j++){
				col = IA[j]&(NVirtual-1);
				temp[i] += L[j] * v[col];
			}
		}
	}

#ifdef DEBUG
	print_vector(temp, M);
#endif

	return omp_get_wtime()-start_time;
}

double tranVectorMultiply()
{
	double *temp;
	temp = (double *)malloc(N * sizeof(double *));

	clock_t transvector_start, transvector_end;
	double transvector_cpu_time_used;
	transvector_start = clock();
	int maxRow = 0, maxCol = 0;

	for(int i = 0; i< nnz; i++){
		temp[i] = 0;
	}   
	for (int i = 0; i < nnz; i++) {
		// Need to change : temp[JA[i]] += L[i] * v[IA[i]];
	}
#ifdef DEBUG
	print_vector(temp, M);
#endif
	transvector_end = clock();
	transvector_cpu_time_used = ((double)(transvector_end - transvector_start) / CLOCKS_PER_SEC) * 1000;
	return transvector_cpu_time_used;
}

void init(char *filename, char *vectorfile)
{
	double sp_time = sparesify(filename);
	//double scal_time = scalarMultiply(q);
	vectorFile(vectorfile);
	double vector_time = vectorMultiply();
	//double tranVector_time = tranVectorMultiply();

#ifdef TIME

    int threads;
#pragma omp parallel 
    threads = omp_get_num_threads();

    printf("IDX SpMV performance: %g GFlops\n", (double)(1.0e-9*threads*2*nnz)/(vector_time));
	// printf("Time Taken for Transpose Matrix-Vector Multiplication = %f MilliSecond\n", tranVector_time);
#endif

	free(IA);
	free(L);
}
