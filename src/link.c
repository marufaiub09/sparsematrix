#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "header/link.h"
int M, N, nnz;
double *JA;
double *IA;

struct Node
{
    int value;
    int row_position;
    int column_postion;
    struct Node *next;
};

double sparesify(FILE *fp)
{
    int i, j, x;
    if (fp == NULL)
    {
        printf("Could not open file");
    }

    fscanf(fp, "%d %d %d", &M, &N, &nnz);

    JA = (double *)malloc(nnz * sizeof(double *));
    IA = (double *)malloc(nnz * sizeof(double *));
    L = (double *)malloc(nnz * sizeof(double *));

    clock_t sp_start, sp_end;
    double sp_cpu_time_used;
    struct Node *start = NULL;
    sp_start = clock();
    for (x = 0; x < nnz; x++)
    {
        fscanf(fp, "%lg %lg %lg", &IA[c], &JA[d], &L[z]);
        //create_new_node(&start, S[c][d], IA[c], JA[d]);
        z++;
        c++;
        d++;
    }
    sp_end = clock();
    sp_cpu_time_used = ((double)(sp_end - sp_start) / CLOCKS_PER_SEC);
    return sp_cpu_time_used;
}

// Function to create new node
void create_new_node(struct Node **start, int non_zero_element,
                     int row_index, int column_index)
{
    struct Node *temp, *r;
    temp = *start;
    if (temp == NULL)
    {
        // Create new node dynamically
        temp = (struct Node *)malloc(sizeof(struct Node));
        temp->value = non_zero_element;
        temp->row_position = row_index;
        temp->column_postion = column_index;
        temp->next = NULL;
        *start = temp;
    }
    else
    {
        while (temp->next != NULL)
            temp = temp->next;

        // Create new node dynamically
        r = (struct Node *)malloc(sizeof(struct Node));
        r->value = non_zero_element;
        r->row_position = row_index;
        r->column_postion = column_index;
        r->next = NULL;
        temp->next = r;
    }
}

// This function prints contents of linked list
// starting from start
void PrintList(struct Node *start)
{
    struct Node *temp, *r, *s;
    temp = r = s = start;

    //printf("row_position: ");
    while (temp != NULL)
    {

        //printf("%d ", temp->row_position);
        temp = temp->next;
    }
    //printf("\n");

    //printf("column_postion: ");
    while (r != NULL)
    {
        //printf("%d ", r->column_postion);
        r = r->next;
    }
    //printf("\n");
    // printf("Value: ");
    while (s != NULL)
    {
        //printf("%d ", s->value);
        s = s->next;
    }
    printf("\n");
}

double scalarMultiply(FILE *fp, double *L)
{
    int i;
    int q = 11;

    if (fp == NULL)
    {
        printf("Could not open file");
    }

    fscanf(fp, "%d %d %d", &M, &N, &nnz);
    double *P = (double *)malloc(nnz * sizeof(double *));
    clock_t scal_start, scal_end;
    double scal_cpu_time_used;
    scal_start = clock();
    for (i = 0; i < z; i++)
    {
        P[i] = q * L[i];
    }

    scal_end = clock();
    scal_cpu_time_used = ((double)(scal_end - scal_start) / CLOCKS_PER_SEC) * 1000;
    return scal_cpu_time_used;
    return 0;
}
double vectorMultiply(FILE *fp, double *L, double *IA, double *JA)
{
    if (fp == NULL)
    {
        printf("Could not open file");
    }

    fscanf(fp, "%d %d %d", &M, &N, &nnz);
    double *VP = (double *)malloc(nnz * sizeof(double *));
    int i, k = 0;
    int j;
    clock_t vector_start, vector_end;
    double vector_cpu_time_used;

    vector_start = clock();

    int *vector = (int *)malloc(nnz * sizeof(int *));

    for (j = 0; j < z; j++)
    {
        vector[k] = JA[j];
        k++;
    }
    for (i = 0; i < M; i++)
    {
        VP[i] = 0;
        for (i = 0; i < z; i++)
        {
            VP[i] = IA[i] + L[i] * JA[vector[i]];
        }
    }
    vector_end = clock();
    vector_cpu_time_used = ((double)(vector_end - vector_start) / CLOCKS_PER_SEC) * 1000;
    return vector_cpu_time_used;
    free(IA);
    free(JA);
    free(L);
}

double tranVectorMultiply(FILE *fp, double *L, double *IA, double *JA)
{

    if (fp == NULL)
    {
        printf("Could not open file");
    }

    fscanf(fp, "%d %d %d", &M, &N, &nnz);
    double *TVP = (double *)malloc(nnz * sizeof(double *));
    int *Tvector = (int *)malloc(nnz * sizeof(int *));
    double *trans = (double *)malloc(nnz * sizeof(double *));
    int i, j, k = 0, t = 0;

    clock_t tranvector_start, tranvector_end;
    double tranvector_cpu_time_used;

    tranvector_start = clock();
    

    for (j = 0; j < z; j++)
    {
        Tvector[k] = JA[j];
        k++;
    }
    for (j = 0; j < 6; ++j)
    {
        for (i = 0; i < 6; ++i)
        {
            trans[t] = L[j + i * 6];

            t++;
        }
    }
    for (i = 0; i < M; i++)
    {
        TVP[i] = 0;
        for (i = 0; i < z; i++)
        {
            TVP[i] = IA[i] + trans[i] * JA[Tvector[i]];
        }
    }
    tranvector_end = clock();
    tranvector_cpu_time_used = ((double)(tranvector_end - tranvector_start) / CLOCKS_PER_SEC) * 1000;
    return tranvector_cpu_time_used;
    free(IA);
    free(JA);
    free(L);
}
