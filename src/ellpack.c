#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "header/ellpack.h"
#include "header/common.h"
#include <assert.h>
#include <omp.h>

int maxColumn(char *filename)
{
    FILE *fp;
    fp = fopen(filename, "r");

    if (fp == NULL) {
        printf("Could not open file!!!");
        exit(1);
    }

    fscanf(fp, "%d %d %d", &M, &N, &nnz);

    int count[N];
    for (int i = 0; i < N; i++){
        count[i]=0;
    } 
    
    int col;
    double temp;
    for (int x = 0; x < nnz; x++) {
        fscanf(fp, "%lg %d %lg\n ", &temp, &col, &temp);
        count[col]++;
    }

    int maxCol = 0;
    for (int i = 0; i < N; i++){
        if (count[i]>maxCol){
            maxCol= count[i];
        } 
    } 

    fclose(fp);

    return maxCol;
    
}

double sparesify(char *filename)
{
    int maxCol = maxColumn(filename);
    
    FILE *fp;
    fp = fopen(filename, "r");

    if (fp == NULL){
        printf("Could not open file!!");
        exit(0);
    }

    fscanf(fp, "%d %d %d", &M, &N, &nnz);
    
    data = (double **)malloc(M * sizeof(double *));
    for (int i = 0; i < M; i++){
        data[i] = (double *)malloc(maxCol * sizeof(double *));
            //for(int j=0; j<= maxCol; j++) data[i][j] = 0;
    }
    column = (int **)malloc(M * sizeof(int *));
    for (int i = 0; i < M; i++){
        column[i] = (int *)malloc(maxCol * sizeof(int *));
	for(int j=0; j<= maxCol; j++) column[i][j] = -1;
    }
    
    
#ifdef SIZE
    int storage =  M*(sizeof(int *) * maxCol*sizeof(int *) + M*sizeof(double *) * maxCol*sizeof(double *));
    printf("Total Storage for ELLPACK : %d  bytes \n", storage);
#endif
    int row_old = 0, row_new=0, c = 0;
    for (int i = 0; i < nnz; i++){
        fscanf(fp, "%d", &row_new);
	    if (row_new-1 == row_old) {
           	c++;
	    } else {
		    row_old=row_new-1;
	        c=0;
        }
        fscanf(fp, "%d %lg", &column[row_old][c], &data[row_old][c]);
    }
    fclose(fp);
}

double scalarMultiply(int q, char *filename)
{
    int maxCol = maxColumn(filename);
    FILE *fp;
    fp = fopen(filename, "r");

    if (fp == NULL){
        printf("Could not open file!!");
        exit(0);
    }
    fscanf(fp, "%d %d %d", &M, &N, &nnz);

    double **P = (double **)malloc(M * (maxCol)  * sizeof(double *));
    for (int i = 0; i < M; i++){
        P[i] = (double *)malloc((maxCol) * sizeof(double *));
        for(int j=0; j<= maxCol; j++) P[i][j] = -1;
    }

    for (int i = 0; i < M; i++) {
        for (int j = 0; j < (maxCol) ; j++) {
            if(data[i][j] != -1 && data[i][j] != 0){
                P[i][j] = q * data[i][j];
            }
        }
    }
    fclose(fp);
}

double vectorFile(char *vectorfile)
{
    FILE *vp;
    vp = fopen(vectorfile, "r");

    if (vp == NULL){
        printf("Could not open file!!");
        exit(0);
    }
    int temp;
    fscanf(vp, "%d ", &temp);
        
    v = (double *)malloc(N * sizeof(double *));
    for (int x = 0; x < N; x++){
        fscanf(vp, "%lg ", &v[x]);
    }
}

double vectorMultiply(char *filename)
{
    int maxCol = maxColumn(filename);    
    double *temp = (double *)malloc(M*maxCol * sizeof(double *));
    
    double start_time = omp_get_wtime(); 
    int  i, j=0;
    #pragma omp parallel for private(i, j), shared(temp, data, v, column)
    for (i = 0; i < M; i++ ){
        temp[i] = 0;
        for(j = 0; j<maxCol; j++){
	    if(column[i][j] == -1) break;
            temp[i] += data[i][j] * v[column[i][j]];
        }
    }
#ifdef DEBUG
    print_vector(temp, M);
#endif
    return omp_get_wtime()-start_time;
}

double tranVectorMultiply(double *v, char *filename)
{
    int maxCol = maxColumn(filename);

    FILE *fp;
    fp = fopen(filename, "r");

    if (fp == NULL){
        printf("Could not open file!!");
        exit(0);
    }

    fscanf(fp, "%d %d %d", &M, &N, &nnz);

    double *temp = (double *)malloc(nnz * sizeof(double *));
    JA = (int *)malloc(nnz * sizeof(int *));
    L = (double *)malloc(nnz * sizeof(double *));

    int *IA = (int *)malloc(nnz * sizeof(int *));

    clock_t transvector_start, transvector_end;
	double transvector_cpu_time_used;
    transvector_start = clock();
    
    int x =0, col = 0 , j=0 ;
    for (int i = 0; i <maxCol+1; i++ ){
        for(int j = 0; j<M; j++){
            if(data[j][i] != -1){
                JA[x] = i;
                IA[x] = j;
                L[x] = data[j][i];
                x++; 
            }
        }
    }
    for (int i = 0; i <M; i++ ){
        temp[i] = 0;
        for(; i == JA[j]; j++){
            temp[i] += L[j] * v[IA[j]];
        }
    }
    #ifdef DEBUG
    print_vector(temp, maxCol);
    #endif
    transvector_end = clock();
	transvector_cpu_time_used = ((double)(transvector_end - transvector_start) / CLOCKS_PER_SEC) * 1000;
	return transvector_cpu_time_used;
}

void init(char *filename,char *vectorfile)
{
    
    double sp_time = sparesify(filename);
    //double scal_time = scalarMultiply(q, filename);
    vectorFile(vectorfile);
    double vector_time = vectorMultiply(filename);
    //double tranVector_time = tranVectorMultiply(v, filename);
#ifdef TIME
    int threads;
#pragma omp parallel
    threads = omp_get_num_threads();

    printf("ELL SpMV performance: %lf GFlops\n", (double)(1.0e-9*threads*2*nnz)/(vector_time));
        // printf("Time Taken for Transpose Matrix-Vector Multiplication = %f MilliSecond\n", tranVector_time);
#endif
    free(data);
    free(column);
    
}
