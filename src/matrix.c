#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "header/matrix.h"
#include "header/common.h"
#include <assert.h>
#include <omp.h>

int maxColumn(char *filename)
{
    FILE *fp;
    fp = fopen(filename, "r");

    if (fp == NULL) {
        printf("Could not open file!!!");
        exit(1);
    }

    fscanf(fp, "%d %d %d", &M, &N, &nnz);
    
    int count[N];
    for (int i = 0; i < N; i++){
        count[i]=0;
    } 
    
    int col;
    double temp;
    for (int x = 0; x < nnz; x++) {
        fscanf(fp, "%lg %d %lg\n ", &temp, &col, &temp);
        count[col]++;
    }

    int maxCol = 0;
    for (int i = 0; i < N; i++){
        if (count[i]>maxCol){
            maxCol= count[i];
        } 
    } 
    fclose(fp);
    printf("| Matrix Data Characteristics     |\n");
    printf("----------------------------------\n");
    printf("| 1| Max Coloumn           |--| %d\n", maxCol);
    printf("----------------------------------\n");
    printf("| 2| Total NonZero Value   |--| %d\n", nnz);
    printf("----------------------------------\n");
}

double sparesify(char *filename)
{
    FILE *fp;
    fp = fopen(filename, "r");

    if (fp == NULL){
        printf("Could not open file!!");
        exit(0);
    }

    fscanf(fp, "%d %d %d", &M, &N, &nnz);
    
    data = (double **)malloc(M * sizeof(double *));
    for (int i = 0; i < M; i++){
        data[i] = (double *)malloc(N * sizeof(double *));
            //for(int j=0; j<= maxCol; j++) data[i][j] = 0;
    }

    tdata = (double **)malloc(M * sizeof(double *));
    for (int i = 0; i < M; i++){
        tdata[i] = (double *)malloc(N * sizeof(double *));
            //for(int j=0; j<= maxCol; j++) data[i][j] = 0;
    }

    column = (int **)malloc(M * sizeof(int *));
    for (int i = 0; i < M; i++){
        column[i] = (int *)malloc(N * sizeof(int *));
	for(int j=0; j<= N; j++) column[i][j] = -1;
    }

    IA = (int *)malloc(nnz * sizeof(int *));
    JA = (int *)malloc(nnz * sizeof(int *));
    int row_old, row_new, c, col;
    for (int i = 0; i < nnz; i++){
        fscanf(fp, "%d", &row_new);
        IA[i] = row_new;
	    if (row_new-1 == row_old) {
           	c++;
	    } else {
		    row_old=row_new-1;
	        c=0;
        }
        fscanf(fp, "%d %lg", &col, &data[row_old][c]);
        JA[i] = col;
    }
    
    fclose(fp);
}

double isSymmetric()
{
    
    int  i, j;
    for (i = 0; i<M; i++ ){
        for(j = 0; j<N; j++){
            tdata[i][j] = data[j][i];
        }
    }
    int pSym = 1, nSym = 1, hSym = 1, vSym = 1;
    double pCount= 0;

    int up=0, lw=0; 
    for(int i=0; i<M; i++){
        for(int j=0;j<N;j++){
            if(i<j){
                up++;
            }else if(i>j){
                lw++;
            }            
        }
    }
    if(up != lw){
        pSym = 0;
    }else{
        pSym = 1;
    }
    double nCount= 0;
    for (i = 0; i < M; i++ ){
        for(j = 0; j<N; j++){
            if(data[i][j] != tdata[i][j]){
                nSym = 0;
                nCount++;
                break;
            }
        }
    }
    double numeric = (nCount/nnz) * 100;
    double hCount= 0;
    for (int i = 0, k = M - 1; i < M / 2; i++, k--) { 
        // Checking each cell of a column. 
        for (int j = 0; j < N; j++) { 
            // check if every cell is identical 
            if (data[i][j] != data[k][j]) { 
                hSym = 0;
                hCount++; 
                break; 
            } 
        } 
    }
    double horizontal = (hCount/nnz) * 100;
    double vCount= 0;
    for (int i = 0, k = N - 1; i < N / 2; i++, k--) { 
        // Checking each cell of a row. 
        for (int j = 0; j < M; j++) { 
            // check if every cell is identical 
            if (data[i][j] != data[k][j]) { 
                vSym = 0;
                vCount++; 
                break; 
            } 
        } 
    }
    double vertical = (vCount/nnz) * 100;
    

    if(pSym == 1){
        printf("| 3| Pattern Symmetric     |--| YES \n");
        printf("---------------------------------\n");
    }else{
        printf("| 3| Pattern Symmetric     |--| NO  \n");
        printf("---------------------------------\n");
    }

    if(nSym == 1){
        printf("| 4| Numeric Symmetric     |--| YES --| %d%     |\n", 100);
        printf("---------------------------------\n");
    }else{
        printf("| 4| Numeric Symmetric     |--| NO  --| %0.2lg% | \n", numeric);
        printf("---------------------------------\n");
    }

    if(hSym == 1){
        printf("| 5| Horizontal Symmetric  |--| YES --| %d%     |\n", 100);
        printf("---------------------------------\n");
    }else{
        printf("| 5| Horizontal Symmetric  |--| NO  --| %0.2lg% |\n", horizontal);
        printf("---------------------------------\n");
    }

    if(vSym == 1){
        printf("| 6| Vertical Symmetric    |--| YES --| %d%     |\n", 100);
        printf("---------------------------------\n");
    }else{
        printf("| 6| Vertical Symmetric    |--| NO  --| %0.2lg% |\n", vertical);
        printf("---------------------------------\n");
    }
       
}
int isSort(filename)
{
    int rOrder = 1, cOrder = 1;
    for(int i = 0; i<nnz-1; i++){
        if(IA[i]>IA[i+1]){
            rOrder = 0;
            break;
        }
    }
    for(int i = 0; i<nnz-1; i++){
        if(JA[i]>JA[i+1]){
            cOrder = 0;
            break;
        }
    }
    if(cOrder == 1){
        printf("| 7| Column Major Order    |--| YES \n");
        printf("---------------------------------\n");
    }else if(rOrder == 1){
        printf("| 7| Row Major Order       |--| YES \n");
        printf("---------------------------------\n");
    }else if(cOrder == 0){
        printf("| 7| Column Major Order    |--| NO \n");
        printf("---------------------------------\n");
    }else if(rOrder == 0){
        printf("| 7| Row Major Order       |--| NO \n");
        printf("---------------------------------\n");
    }else{
        printf("| 7| Problem Found. \n");
        printf("---------------------------------\n");
    }

    // if(cOrder == 1){
    //         printf("| 7| Column Major Order    |--| YES \n");
    //         printf("---------------------------------\n");
    //     }else{
    //         printf("| 7| Column Major Order    |--| NO \n");
    //         printf("---------------------------------\n");
    // }
    // if(rOrder == 1){
    //         printf("| 8| Row Major Order       |--| YES \n");
    //         printf("---------------------------------\n");
    //     }else{
    //         printf("| 8| Row Major Order       |--| NO \n");
    //         printf("---------------------------------\n");
    // }
}
double density()
{
    double rowCol = M*N;
    int zeroElement = rowCol - nnz;
    int spars = zeroElement/rowCol;
    float avg = 0;
    avg = (nnz+zeroElement) / M;
    printf("| 2| Total Average Value   |--| %0.2f\n", avg);
    printf("----------------------------------\n");
    printf("| 8| Total Sparsify        |--| %0.4lg\n", zeroElement/rowCol);
    printf("-----------------------------------\n");
    printf("| 9| Total Density         |--| %0.4g\n", nnz/rowCol);
    printf("----------------------------------\n");
    
    double up=0, lw=0,dia=0;
    if(M == N){
        for(int i=0; i<M; i++){
            for(int j=0;j<N;j++){
                if(i<j){
                    up++;
                }else if(i>j){
                    lw++;
                }else if(i==j){
                    dia++;
                }
            }
        }
    }else{
        printf("Total Row and Total Column Are Not Same.\n");
        exit(0);
    }
    
    printf("|10| Upper Triangle Density|--| %0.4g\n", nnz/up);
    printf("----------------------------------\n");
    printf("|11| Lower Triangle Density|--| %0.4g\n", nnz/lw);
    printf("----------------------------------\n");
    printf("|12| Diagonal Density      |--| %0.4g\n", nnz/dia);
    printf("----------------------------------\n");
}

    

void init(char *filename,char *vectorfile)
{
    maxColumn(filename);
    sparesify(filename);
    isSymmetric();
    isSort();
    density();

    free(data);
    free(column);
    free(IA);
    free(JA);
}
