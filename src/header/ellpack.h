#ifndef MY_ellpack_H
#define MY_ellpack_H

#include <stdio.h>

double **data;
int **column;
int M, N, nnz;
double *L, *v;
int *JA;

int maxColumn(char *filename);
double sparesify(char *filename);
double scalarMultiply(int q, char *filename);
double vectorFile(char *vectorfile);
double vectorMultiply(char *filename);
double tranVectorMultiply(double *v, char *filename);
void init(char *filename,char *vectorfile);

#endif
