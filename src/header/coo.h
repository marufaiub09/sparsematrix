#ifndef MY_Coo_H
#define MY_Coo_H
#include <stdio.h>

double *L , *v;
int *JA, *IA;
int M, N, nnz;

double sparesify(char *filename);
double scalarMultiply(int q);
void vectorFile(char *vectorfile);
double vectorMultiply();
double tranVectorMultiply();

void init(char *filename,char *vectorfile);

#endif
