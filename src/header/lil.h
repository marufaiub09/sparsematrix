#ifndef MY_lil_H
#define MY_lil_H
#include <stdio.h>
#include <stdlib.h>



struct row_list;
struct value_list;
double sparesify(FILE *fp);
void create_value_node(int data, int j, struct row_list **h);
void create_row_list(struct row_list **start, int row,int column, int **S);
void print_LIL(struct row_list *start);
double scalarMultiply(FILE *fp, double *L);
double vectorMultiply(FILE *fp, double *L, double *IA, double *JA);
double tranVectorMultiply(FILE *fp, double *L, double *IA, double *JA);
#endif