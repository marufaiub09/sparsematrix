#ifndef MY_csr_H
#define MY_csr_H

#include<stdio.h>

double *L, *v;
int *JA, *row_ptr;
int M, N, nnz;

double sparesify(char *filename);
double scalarMultiply(int q);
void vectorFile(char *vectorfile);
double vectorMultiply();
double tranVectorMultiply();

void init(char *filename,char *vectorfile);

#endif
