#ifndef MY_COMMOM_H
#define MY_COMMON_H

#include<stdio.h>

void gen_vector(char *filename, int size)
{
    FILE *fp;
    fp = fopen(filename, "w");

    if (fp == NULL){
        printf("Could not open file to write!!");
        exit(0);
    }

    fprintf(fp, "%d 1\n", size);
    for(int i=0; i<size; i++)
    	fprintf(fp, "%d\n", rand());

    fclose(fp);
}

double print_vector(double *v, int size)
{
   for(int i=0; i<size; i++) {
	printf("%e\n", v[i]);
   }
   printf("\n");
   printf("\n");
}

double print_vector_int(long *v, int size)
{
   for(int i=0; i<size; i++) {
	printf("%ld\n", v[i]);
   }
   printf("\n");
   printf("\n");
}

double print_matrix(double **v, int r_size, int c_size)
{
   for(int i=0; i<r_size; i++) {
      for(int j=0; j<c_size; j++){
         printf("%lg ", v[i][j]);
      }
      printf("\n");
   }
   printf("\n");
}

double print_matrix_int(int **v, int r_size, int c_size)
{
   for(int i=0; i<r_size; i++) {
      for(int j=0; j<c_size; j++){
         printf("%d ", v[i][j]);
      }
      printf("\n");
   }
   printf("\n");
}

#endif
