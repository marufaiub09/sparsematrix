#ifndef MY_Link_H
#define MY_Link_H
#include<stdio.h>


struct Node;

double sparesify(FILE *fp);
void create_new_node(struct Node** start, int non_zero_element,int row_index, int column_index );
void PrintList(struct Node* start);
double scalarMultiply(FILE *fp, double *L);
double vectorMultiply(FILE *fp, double *L, double *IA, double *JA);
double tranVectorMultiply(FILE *fp, double *L, double *IA, double *JA);
#endif