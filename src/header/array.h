#ifndef MY_Array_H
#define MY_Array_H
#include <stdio.h>

int M, N, nnz;
int *JA;
int *IA;
double *L;

double sparesify(FILE *fp);
double scalarMultiply(int q);
double vectorMultiply(double *v);
double tranVectorMultiply(double *v);
#endif