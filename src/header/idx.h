#ifndef MY_IDX_H
#define MY_IDX_H
#include <stdio.h>

double *L , *v;
long *IA;
int *row_ptr;
int M, N, nnz;

double sparesify(char *filename);
double scalarMultiply(int q);
void vectorFile(char *vectorfile);
double vectorMultiply();
double tranVectorMultiply();

void init(char *filename,char *vectorfile);

#endif
