#ifndef MY_ellpack_H
#define MY_ellpack_H

#include <stdio.h>

double **data;
double **tdata;
int **column;
int M, N, nnz;

int *IA, *JA;

int maxColumn(char *filename);
double sparesify(char *filename);
double isSymmetric();
int isSort();
double density();

void init(char *filename,char *vectorfile);

#endif
