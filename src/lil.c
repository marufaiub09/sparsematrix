#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "header/lil.h"

int M, N, nnz;
double *JA;
double *IA;

// Node to represent row - list
struct row_list
{
	int row_number;
	struct row_list *link_down;
	struct value_list *link_right;
};

// Node to represent triples
struct value_list
{
	int column_index;
	int value;
	struct value_list *next;
};

double sparesify(FILE *fp)
{
	int i, j, x;
    if (fp == NULL)
    {
        printf("Could not open file");
    }

    fscanf(fp, "%d %d %d", &M, &N, &nnz);

    JA = (double *)malloc(nnz * sizeof(double *));
    IA = (double *)malloc(nnz * sizeof(double *));
    L = (double *)malloc(nnz * sizeof(double *));

	clock_t sp_start, sp_end;
	double sp_cpu_time_used;
	sp_start = clock();

	for (x = 0; x < nnz; x++)
    {
        fscanf(fp, "%lg %lg %lg", &IA[c], &JA[d], &L[z]);
        //create_new_node(&start, S[c][d], IA[c], JA[d]);
        z++;
        c++;
        d++;
    }
	
	sp_end = clock();
	sp_cpu_time_used = ((double)(sp_end - sp_start)/CLOCKS_PER_SEC);
	return sp_cpu_time_used;
}

// Fuction to create node for non - zero elements
void create_value_node(int data, int j, struct row_list **h)
{
	struct value_list *temp, *d;

	// Create new node dynamically
	temp = (struct value_list *)malloc(sizeof(struct value_list));
	temp->column_index = j + 1;
	temp->value = data;
	temp->next = NULL;

	// Connect with row list
	if ((*h)->link_right == NULL)
		(*h)->link_right = temp;
	else
	{
		// d points to data list node
		d = (*h)->link_right;
		while (d->next != NULL)
			d = d->next;
		d->next = temp;
	}
}

// Function to create row list
void create_row_list(struct row_list **starts, int row,
					 int column, int **S)
{
	// For every row, node is created
	for (int i = 0; i < row; i++)
	{
		struct row_list *h, *r;

		// Create new node dynamically
		h = (struct row_list *)malloc(sizeof(struct row_list));
		h->row_number = i + 1;
		h->link_down = NULL;
		h->link_right = NULL;
		if (i == 0)
			*starts = h;
		else
		{
			r = *starts;
			while (r->link_down != NULL)
				r = r->link_down;
			r->link_down = h;
		}

		// Firstiy node for row is created,
		// and then travering is done in that row
		for (int j = 0; j < M; j++)
		{
			if (S[i][j] != 0)
			{
				create_value_node(S[i][j], j, &h);
			}
		}
	}
}

//Function display data of LIL
void print_LIL(struct row_list *starts)
{
	struct row_list *r;
	struct value_list *h;
	r = starts;

	// Traversing row list
	while (r != NULL)
	{
		if (r->link_right != NULL)
		{
			//printf("row=%d \n", r->row_number);
			h = r->link_right;

			// Traversing data list
			while (h != NULL)
			{
				//printf("column=%d value=%d \n",
					  // h->column_index, h->value);

				// L[z] = h->value;
				// z++;
				h = h->next;
			}
		}
		r = r->link_down;
	}
}

double scalarMultiply(FILE *fp, double *L)
{
    int i;
    int q = 11;

    if (fp == NULL)
    {
        printf("Could not open file");
    }

    fscanf(fp, "%d %d %d", &M, &N, &nnz);
    double *P = (double *)malloc(nnz * sizeof(double *));
    clock_t scal_start, scal_end;
    double scal_cpu_time_used;
    scal_start = clock();
    for (i = 0; i < z; i++)
    {
        P[i] = q * L[i];
    }

    scal_end = clock();
    scal_cpu_time_used = ((double)(scal_end - scal_start) / CLOCKS_PER_SEC) * 1000;
    return scal_cpu_time_used;
    return 0;
}
double vectorMultiply(FILE *fp, double *L, double *IA, double *JA)
{
    if (fp == NULL)
    {
        printf("Could not open file");
    }

    fscanf(fp, "%d %d %d", &M, &N, &nnz);
    double *VP = (double *)malloc(nnz * sizeof(double *));
    int i, k = 0;
    int j;
    clock_t vector_start, vector_end;
    double vector_cpu_time_used;

    vector_start = clock();

    int *vector = (int *)malloc(nnz * sizeof(int *));

    for (j = 0; j < z; j++)
    {
        vector[k] = JA[j];
        k++;
    }
    for (i = 0; i < M; i++)
    {
        VP[i] = 0;
        for (i = 0; i < z; i++)
        {
            VP[i] = IA[i] + L[i] * JA[vector[i]];
        }
    }
    vector_end = clock();
    vector_cpu_time_used = ((double)(vector_end - vector_start) / CLOCKS_PER_SEC) * 1000;
    return vector_cpu_time_used;
    free(IA);
    free(JA);
    free(L);
}

double tranVectorMultiply(FILE *fp, double *L, double *IA, double *JA)
{

    if (fp == NULL)
    {
        printf("Could not open file");
    }

    fscanf(fp, "%d %d %d", &M, &N, &nnz);
    double *TVP = (double *)malloc(nnz * sizeof(double *));
    int *Tvector = (int *)malloc(nnz * sizeof(int *));
    double *trans = (double *)malloc(nnz * sizeof(double *));
    int i, j, k = 0, t = 0;

    clock_t tranvector_start, tranvector_end;
    double tranvector_cpu_time_used;

    tranvector_start = clock();
    

    for (j = 0; j < z; j++)
    {
        Tvector[k] = JA[j];
        k++;
    }
    for (j = 0; j < 6; ++j)
    {
        for (i = 0; i < 6; ++i)
        {
            trans[t] = L[j + i * 6];

            t++;
        }
    }
    for (i = 0; i < M; i++)
    {
        TVP[i] = 0;
        for (i = 0; i < z; i++)
        {
            TVP[i] = IA[i] + trans[i] * JA[Tvector[i]];
        }
    }
    tranvector_end = clock();
    tranvector_cpu_time_used = ((double)(tranvector_end - tranvector_start) / CLOCKS_PER_SEC) * 1000;
    return tranvector_cpu_time_used;
    free(IA);
    free(JA);
    free(L);
}

