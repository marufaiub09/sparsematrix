#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "header/coo.h"
#include "header/common.h"
#include <omp.h>

double sparesify(char *filename)
{
    FILE *fp;
    fp = fopen(filename, "r");

    if (fp == NULL){
        printf("Could not open file!!");
        exit(0);
    }

    fscanf(fp, "%d %d %d", &M, &N, &nnz);

    L = (double *)malloc(nnz * sizeof(double *));
    JA = (int *)malloc(nnz * sizeof(int *));
    IA = (int *)malloc(nnz * sizeof(int *));
    
#ifdef SIZE
    int storage = nnz * (sizeof(int *) + sizeof(int *) + sizeof(double *));
    printf("Total Storage for COO : %d bytes \n", storage);
#endif 
  
    int row, col; 
    for (int x = 0; x < nnz; x++) {
        fscanf(fp, "%d %d %lg\n ", &row, &col, &L[x]);
        IA[x] = row-1; 	// For 0-index array format
        JA[x] = col-1;  // For 0-index array format
    }
    fclose(fp);
}
double scalarMultiply(int q)
{
    double *P;
    P = (double *)malloc(nnz * sizeof(double *));
    for (int i = 0; i < nnz; i++) {
        P[i] = q * L[i];
    }
    #ifdef DEBUG
    print_vector(P, M);
    #endif
}

void vectorFile(char *vectorfile)
{
	FILE *fp;
	fp = fopen(vectorfile, "r");

	if (fp == NULL){
		printf("Could not open file to read vector!!");
		exit(0);
	}

	int temp;
	fscanf(fp, "%d %d", &temp, &temp);

	v = (double *)malloc(N * sizeof(double *));
	for (int x = 0; x < N; x++){
		fscanf(fp, "%lg ", &v[x]);
	}
	
	fclose(fp);
}

double vectorMultiply()
{ 
	double start_time = omp_get_wtime();
	    
    double *temp;
    temp = (double *)malloc(M * sizeof(double *));

    int i;

#pragma omp parallel for private(i), shared(temp, L, v, IA, JA)
    for (i = 0; i < nnz; i++) {
        temp[IA[i]] += L[i] * v[JA[i]];
    }

#ifdef DEBUG
    print_vector(temp, M);
#endif

    return omp_get_wtime()-start_time;
}

double tranVectorMultiply()
{
    double *temp;
    temp = (double *)malloc(N * sizeof(double *));

    clock_t transvector_start, transvector_end;
	double transvector_cpu_time_used;
    transvector_start = clock();
    int maxRow = 0, maxCol = 0;

    for(int i = 0; i< nnz; i++){
        temp[i] = 0;
    }   
    for (int i = 0; i < nnz; i++) {
        temp[JA[i]] += L[i] * v[IA[i]];
    }
#ifdef DEBUG
    print_vector(temp, M);
#endif
    transvector_end = clock();
    transvector_cpu_time_used = ((double)(transvector_end - transvector_start) / CLOCKS_PER_SEC) * 1000;
    return transvector_cpu_time_used;
}

void init(char *filename, char *vectorfile)
{
    double sp_time = sparesify(filename);
    //double scal_time = scalarMultiply(q);
    vectorFile(vectorfile);
    double vector_time = vectorMultiply();
    //double tranVector_time = tranVectorMultiply();

#ifdef TIME
    int threads;
#pragma omp parallel
    threads = omp_get_num_threads();

    printf("COO SpMV performance: %lf GFlops\n", (double)(1.0e-9*threads*2*nnz)/(vector_time));
        // printf("Time Taken for Transpose Matrix-Vector Multiplication = %f MilliSecond\n", tranVector_time);
#endif

    free(IA);
    free(JA);
    free(L);
}
