#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "header/array.h"
#include "header/common.h"

double sparesify(FILE *fp)
{
	if (fp == NULL){
		printf("Could not open file!!");
        return 1;
	}

	fscanf(fp, "%d %d %d", &M, &N, &nnz);

	JA = (int *)malloc(nnz * sizeof(int *));
    IA = (int *)malloc(nnz * sizeof(int *));
    L = (double *)malloc(nnz * sizeof(double *));

	double(*compactMatrix)[3] = malloc(M *N * sizeof(double *));
	if (compactMatrix == NULL){
		fprintf(stderr, "Unable to allocate memory\n");
		exit(EXIT_FAILURE);
	}
	int k = 0;
	for (int x = 0; x < nnz; x++){
		fscanf(fp, "%lg %lg %lg\n ", &compactMatrix[0][k], &compactMatrix[1][k], &compactMatrix[2][k]);
		L[x] = compactMatrix[2][k];
		IA[x] = compactMatrix[0][k];
		JA[x] = compactMatrix[1][k];
	}
}
double scalarMultiply(int q)
{
    double *P = (double *)malloc(nnz * sizeof(double *));

    for (int i = 0; i < nnz; i++) {
        P[i] = q * L[i];
    }
}

double vectorMultiply(double *v)
{
    double *temp = (double *)malloc(nnz * sizeof(double *));
    
    int j = 0;    
    for (int i = 0; i < M; i++) {
        temp[i] = 0;
        for (;i==IA[j]; j++) {
            temp[i] += L[j] * v[JA[j]];
        }
    }
}

double tranVectorMultiply(double *v)
{
    double *temp = (double *)malloc(nnz * sizeof(double *));
    double *trans = (double *)malloc(nnz * sizeof(double *));

    int j = 0;    
    for (int i = 0; i < M; i++) {
        temp[i] = 0;
        for (;i==IA[j]; j++) {
            temp[i] += L[j] * v[JA[j]];
        }
    }
}
void init(FILE *fp, int q, double *v)
{
    double sp_time = sparesify(fp);
    double scal_time = scalarMultiply(q);
    double vector_time = vectorMultiply(v);
    double tranVector_time = tranVectorMultiply(v);
    free(IA);
    free(JA);
    free(L);
}
