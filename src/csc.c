#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "header/csc.h"
#include "header/common.h"
#include <assert.h>
#include <omp.h>

double sparesify(char *filename)
{
    
    FILE *fp;
    fp = fopen(filename, "r");

    if (fp == NULL){
        printf("Could not open file!!");
        exit(0);
    }
    fscanf(fp, "%d %d %d", &M, &N, &nnz);

    L = (double *)malloc(nnz * sizeof(double *));
    IA = (int *)malloc(nnz * sizeof(int *));
    col_ptr = (int *)malloc((N+1) * sizeof(int *));

#ifdef SIZE
    int storage = nnz*(sizeof(double *) + sizeof(int *)) + (N+1)*sizeof(int *);
    printf("Total Storage for CSC : %d bytes \n", storage);
#endif // DEBUG
  
    for (int x = 0; x <= N; x++){
	    col_ptr[x] = 0;
    }
    int row, col;
    for (int x = 0; x < nnz; x++){
        fscanf(fp, "%d %d %lg\n ", &row, &col, &L[x]);
        IA[x] = row-1;		// to maintain 0-index array format
	    col_ptr[col]++;	   // to maintain 0-index array format
        //printf("%d\n", JA[x]);
    }
    for (int x = 1; x <= N; x++){
    	col_ptr[x] = col_ptr[x] + col_ptr[x-1];
    }
    //exit(0);
    //printf("\n");
    //print_vector(L, nnz);
    //print_vector_int(IA, nnz);
    //print_vector_int(col_ptr, N+1);
    fclose(fp);
}

double scalarMultiply(int q)
{
    double *P = (double *)malloc(nnz * sizeof(double *));
    for (int i = 0; i < nnz; i++) {
        P[i] = q * L[i];
    }
    
    #ifdef DEBUG
    print_vector(P, M);
    #endif
}

void vectorFile(char *vectorfile)
{
	FILE *fp;
	fp = fopen(vectorfile, "r");

	if (fp == NULL){
		printf("Could not open file to read vector!!");
		exit(0);
	}

	int temp;
	fscanf(fp, "%d %d", &temp, &temp);

	v = (double *)malloc(N * sizeof(double *));
	for (int x = 0; x < N; x++){
		fscanf(fp, "%lg ", &v[x]);
	}
	
	fclose(fp);
}

double vectorMultiply()
{    
    double start_time = omp_get_wtime();
    double *temp;
    temp = (double *)malloc(N * sizeof(double *));
    
    int j;
#pragma omp parallel for shared(temp), private(j)
    for(j = 0; j < N ; j++){
        temp[j] = 0;
        int i;
        for(i = col_ptr[j]; i < col_ptr[j+1]; i++){
            temp[j] += L[i] * v[IA[i]];
        }
    }

#ifdef DEBUG
    print_vector(temp, M);
#endif

    return omp_get_wtime()-start_time;
}

double tranVectorMultiply()
{
    double *data = (double *)malloc(nnz * sizeof(double*));
    double *tempt = (double *)malloc(nnz * sizeof(double *));
    int new_col = 0, val = 0;
    int maxRow = 0, maxCol = 0;

    /*
    for(int i = 0; i<nnz; i++){
        if(maxRow < IA[i] ){
            maxRow = IA[i];
        }
        if(maxCol < JA[i]){
            maxCol = JA[i];
            
        }
    }
    
    for(int i = 0; i< maxRow+1; i++){
        for(int j = 0; j< maxCol; j++){
            int x = i*maxCol+j;
            int y = j*(maxRow+1)+i;
            IA[y] = j;
            JA[y] = i;
            data[y] =  L[x];
        }
    }
    
    for(int i = 0; i< nnz; i++){
        L[i] = data[i];
    }

    int count = 0, row_old = 0, i=0;
    row_ptr[i++] = count;
    for (int x = 0; x < nnz; x++){
        if(IA[x]!=row_old) {
            row_old = IA[x];
            row_ptr[i++] = count;
            
        }
        count++;
    }
    row_ptr[i] = count;
    
    //print_vector_int(row_ptr, maxCol+1);
    for (int i = 0; i < M; i++) {
        tempt[i] = 0;
    
        for (int j = row_ptr[i];j<row_ptr[i+1]; j++) {
            tempt[i] += L[j] * v[JA[j]];
        }
    }

    */

    #ifdef DEBUG
    print_vector(tempt, M);
    #endif
}

void init(char *filename,char *vectorfile)
{
    double sp_time = sparesify(filename);
    // double scal_time = scalarMultiply(q);
    vectorFile(vectorfile);
    double vector_time = vectorMultiply();
    //double tranVector_time = tranVectorMultiply();

#ifdef TIME
    int threads;
#pragma omp parallel
    threads = omp_get_num_threads();

    printf("CSC SpMV performance: %lf GFlops\n", (double)(1.0e-9*threads*2*nnz)/(vector_time));
#endif
    
    free(IA);
    free(L);
}
