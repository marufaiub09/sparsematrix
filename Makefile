CC=gcc
CFLAGS= -w -O3 -fopenmp
SIZE=$(M)
EXT=.mtx
MATRIX=sherman3
VECTOR=$(MATRIX)_b
DATA_DIR=data/

.PHONY: all clean matrix coo csr csc idx ell clear

sparse: sparse.o
	$(CC)  sparse.o -o sparse

sparse.o: sparse.c
	$(CC) $(CFLAGS) sparse.c
		  

clean:
	echo $(SIZE)
	rm -rf *o sparse

time: coo csr csc idx ell

debug: coo_debug csr_debug csc_debug idx_debug ell_debug
	diff -q coo.dat idx.dat
	diff -q coo.dat csr.dat
	diff -q coo.dat csc.dat
	diff -q coo.dat ell.dat
	
clear: .
	clear

matrix: .
	@$(CC) $(CFLAGS) sparse.c -DMATRIX -DTIME -o matrix
	@./matrix $(DATA_DIR)$(MATRIX)$(EXT) $(DATA_DIR)$(VECTOR)$(EXT)

coo: .
	@$(CC) $(CFLAGS) sparse.c -DCOO -DTIME -o coo
	@./coo $(DATA_DIR)$(MATRIX)$(EXT) $(DATA_DIR)$(VECTOR)$(EXT)

csr: .
	@$(CC) $(CFLAGS) sparse.c -DCSR -DTIME -o csr
	@./csr $(DATA_DIR)$(MATRIX)$(EXT) $(DATA_DIR)$(VECTOR)$(EXT)

csc: .
	@$(CC) $(CFLAGS) sparse.c -DCSC -DTIME -o csc
	@./csc $(DATA_DIR)$(MATRIX)$(EXT) $(DATA_DIR)$(VECTOR)$(EXT)

idx: .
	@$(CC) $(CFLAGS) sparse.c -DIDX -DTIME -o idx
	@./idx $(DATA_DIR)$(MATRIX)$(EXT) $(DATA_DIR)$(VECTOR)$(EXT)
ell: .
	@$(CC) $(CFLAGS) sparse.c -DELL -DTIME -o ell
	@./ell $(DATA_DIR)$(MATRIX)$(EXT) $(DATA_DIR)$(VECTOR)$(EXT)

matrix_debug: .
	$(CC) $(CFLAGS) sparse.c -DMATRIX -DDEBUG -o matrix
	./matrix $(DATA_DIR)$(MATRIX)$(EXT) $(DATA_DIR)$(VECTOR)$(EXT) > matrix.dat

coo_debug: .
	$(CC) $(CFLAGS) sparse.c -DCOO -DDEBUG -o coo
	./coo $(DATA_DIR)$(MATRIX)$(EXT) $(DATA_DIR)$(VECTOR)$(EXT) > coo.dat

csr_debug: .
	$(CC) $(CFLAGS) sparse.c -DCSR -DDEBUG -o csr
	./csr $(DATA_DIR)$(MATRIX)$(EXT) $(DATA_DIR)$(VECTOR)$(EXT) > csr.dat

csc_debug: .
	$(CC) $(CFLAGS) sparse.c -DCSC -DDEBUG -o csc
	./csc $(DATA_DIR)$(MATRIX)$(EXT) $(DATA_DIR)$(VECTOR)$(EXT) > csc.dat

idx_debug: .
	$(CC) $(CFLAGS) sparse.c -DIDX -DDEBUG -o idx
	./idx $(DATA_DIR)$(MATRIX)$(EXT) $(DATA_DIR)$(VECTOR)$(EXT) > idx.dat

ell_debug: .
	$(CC) $(CFLAGS) sparse.c -DELL -DDEBUG -o ell
	./ell $(DATA_DIR)$(MATRIX)$(EXT) $(DATA_DIR)$(VECTOR)$(EXT) > ell.dat
