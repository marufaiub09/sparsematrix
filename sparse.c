#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#ifdef LIL
#include "src/lil.c"
#elif COO
#include "src/coo.c"
#elif IDX
#include "src/idx.c"
#elif ARR
#include "src/array.c"
#elif CSR
#include "src/csr.c"
#elif CSC
#include "src/csc.c"
#elif ELL
#include "src/ellpack.c"
#elif LN
#include "src/link.c"
#elif MATRIX
#include "src/matrix.c"
#endif

int main(int argc, char **argv)
{
    if (argc < 3) {
	printf("Usage ./a.out <mtx filename> <mtx vectorfile>\n");
	exit(0);
    }

    init(argv[1], argv[2]);
    
    return 0;
}

