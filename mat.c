#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "header/matrix.h"
#include "header/common.h"
#include <assert.h>
#include <omp.h>

int maxColumn(char *filename)
{
    FILE *fp;
    fp = fopen(filename, "r");

    if (fp == NULL) {
        printf("Could not open file!!!");
        exit(1);
    }

    fscanf(fp, "%d %d %d", &M, &N, &nnz);
    
    int maxCol = 0, col;
    double temp;
    for (int x = 0; x < nnz; x++) {
        fscanf(fp, "%lg %d %lg\n ", &temp, &col, &temp);
        if(maxCol < col){
            maxCol = col;
        }
    }
    fclose(fp);
    
    return maxCol;
    
}

double sparesify(char *filename)
{
    int maxCol = maxColumn(filename);
    
    FILE *fp;
    fp = fopen(filename, "r");

    if (fp == NULL){
        printf("Could not open file!!");
        exit(0);
    }

    fscanf(fp, "%d %d %d", &M, &N, &nnz);

    printf("Max Col is:%d\n", maxCol);
    printf("Total NNZ values are: %d\n", nnz);
    
    data = (double **)malloc(M * sizeof(double *));
    for (int i = 0; i < M; i++){
        data[i] = (double *)malloc(N * sizeof(double *));
            //for(int j=0; j<= maxCol; j++) data[i][j] = 0;
    }

    tdata = (double **)malloc(M * sizeof(double *));
    for (int i = 0; i < M; i++){
        tdata[i] = (double *)malloc(N * sizeof(double *));
            //for(int j=0; j<= maxCol; j++) data[i][j] = 0;
    }

    column = (int **)malloc(M * sizeof(int *));
    for (int i = 0; i < M; i++){
        column[i] = (int *)malloc(N * sizeof(int *));
	for(int j=0; j<= N; j++) column[i][j] = -1;
    }

    IA = (int *)malloc(nnz * sizeof(int *));

    int row_old = 0, row_new=0, c = 0;
    for (int i = 0; i < nnz; i++){
        fscanf(fp, "%d", &row_new);
        IA[i] = row_new -1;
	    if (row_new-1 == row_old) {
           	c++;
	    } else {
		    row_old=row_new-1;
	        c=0;
        }
        fscanf(fp, "%d %lg", &column[row_old][c], &data[row_old][c]);
    }
    fclose(fp);
}

double isSymmetric(char *filename)
{
    
    int  i, j=0;
    // for (i = 0; i<M; i++ ){
    //     for(j = 0; j<N; j++){
    //         tdata[i][j] = data[j][i];
    //         printf("%lg", tdata[i][j]);
    //     }
    //     printf("\n");
    // }
    for (i = 0; i<M; i++ ){
        for(j = 0; j<N; j++){
            tdata[i][j] = data[i][j];
            printf("%lg", tdata[i][j]);
        }
        printf("\n");
    }
    // int isSym = 1;
    // for (i = 1; i < M; i++ ){
    //     for(j = 1; j<N; j++){
    //         if(data[i][j] != tdata[i][j]){
    //             isSym = 0;
    //             break;
    //         }
    //     }
    // }
    // if(isSym == 1){
    //      printf("The given matrix data is  Symmetric matrix.\n");
    // }else{
    //     printf("The given matrix data is not Symmetric matrix.\n");
    // }
}
int isSort(filename)
{
    for(int i = 0; i<nnz-1; i++){
        if(IA[i]>IA[i+1]){
            printf("Array is not Stored in Assending Order.\n");
            return;
        }
    }
    printf("Array is Sorted in Assending Order.\n");
}

    

void init(char *filename,char *vectorfile)
{
    
    sparesify(filename);
    isSymmetric(filename);
    isSort(filename);

    free(data);
    free(column);
    
}
