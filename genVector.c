#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    if (argc < 3) {
	printf("Usage ./a.out <ouput mtx filename> <vector size>\n");
	exit(0);
    }

    FILE *fp;
    fp = fopen("data/add20_b.mtx", "r");

    if (fp == NULL){
        printf("Could not open data file to read!!");
        exit(0);
    }

    int x;
    fscanf(fp, "%d %d", &x, &x);

    double num[2395];
   
    for(x=0; x<2395; x++) {
	fscanf(fp, "%le", &num[x]);
    }
    fclose(fp);

    fp = fopen(argv[1], "w");

    if (fp == NULL){
        printf("Could not open file to write!!");
        exit(0);
    }

    int size = atoi(argv[2]);
    fprintf(fp, "%d 1\n", size);
    for(int i=0; i<size; i++) {
        int idx = rand()%2395;
    	fprintf(fp, "%.12le\n", num[idx]);
    }

    fclose(fp);
}

